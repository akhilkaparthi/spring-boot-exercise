package com.clinpal.exercise.service.contact.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.Test;

class ContactTest {

    private static final String STREET = "345 Cave Stone Road";
    private static final String SURNAME = "Flintstone";
    private static final String FORENAME = "Fred";
    private static final String CITY = "Bedrock";
    private static final String POSTAL_CODE = "BR 165";
    private static final String COUNTY = "Cobblestone County";
    private static final String COUNTRY = "USA";
    private final Contact contact = new Contact();

    @Test
    void setId() {
        final long id = System.currentTimeMillis();
        contact.setId(id);
        assertThat(contact.getId(), is(id));
    }

    @Test
    void setForename() {
        contact.setForename(FORENAME);
        assertThat(contact.getForename(), is(FORENAME));
    }

    @Test
    void setSurname() {
        contact.setSurname(SURNAME);
        assertThat(contact.getSurname(), is(SURNAME));
    }

    @Test
    void setStreet() {
        contact.setStreet(STREET);
        assertThat(contact.getStreet(), is(STREET));
    }

    @Test
    void setCity() {
        contact.setCity(CITY);
        assertThat(contact.getCity(), is(CITY));
    }

    @Test
    void setPostalCode() {
        contact.setPostalCode(POSTAL_CODE);
        assertThat(contact.getPostalCode(), is(POSTAL_CODE));
    }

    @Test
    void setCounty() {
        contact.setCounty(COUNTY);
        assertThat(contact.getCounty(), is(COUNTY));
    }

    @Test
    void setCountry() {
        contact.setCountry(COUNTRY);
        assertThat(contact.getCountry(), is(COUNTRY));
    }


}
