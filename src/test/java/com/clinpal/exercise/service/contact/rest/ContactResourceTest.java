package com.clinpal.exercise.service.contact.rest;

import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * I have added a few test cases for demonstration
 * more test cases are possible
 */
@SpringBootTest
public class ContactResourceTest {

    private static final String CONTACTS_END_POINT = "/api/contacts";

    private int port = 5000;

    TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private ContactService contactService;

    @Test
    public void testAddContact() {
        Contact contact = getNewTestContact();
        HttpEntity<Contact> entity = new HttpEntity<>(contact);
        System.out.println("in test method "+createURLWithPort(CONTACTS_END_POINT));
        ResponseEntity<Contact> response = restTemplate.exchange(
                createURLWithPort(CONTACTS_END_POINT),
                HttpMethod.POST, entity, Contact.class);

        assertThat(response.getStatusCode().value(), is(200));

    }
    private Contact getNewTestContact() {
        Contact contact = new Contact();
        contact.setForename("K");
        contact.setSurname("Ak");
        contact.setStreet("289");
        contact.setCity("london");
        contact.setPostalCode(("tw5 0de"));
        contact.setCounty("middlesex");
        contact.setCountry("uk");
        return contact;

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
