package com.clinpal.exercise.service.contact.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsIterableContaining.hasItems;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

@SpringBootTest
class ContactServiceImplTest {

    @Autowired
    private ContactService contactService;

    @Test
    void findContactBySurnameLike() {

        final Contact fred = new Contact();
        fred.setForename("Fred");
        fred.setSurname("Flintstone");
        contactService.save(fred);

        final Contact wilma = new Contact();
        wilma.setForename("Wilma");
        wilma.setSurname("Flintstone");
        contactService.save(wilma);

        final Contact barney = new Contact();
        barney.setForename("Barney");
        barney.setSurname("Rubble");
        contactService.save(barney);

        assertThat(contactService.findContactBySurnameLike("Flint"), hasItems(fred, wilma));

    }

    @Test
    void update() {
        Contact contact = getNewTestContact();
        contactService.save(contact);

        Contact findContact = contactService.findById(contact.getId());
        findContact.setForename("Barney");
        findContact.setSurname("Rubble");
        contactService.update(contact.getId(),findContact);

        assertThat(findContact.getSurname(), is("Rubble"));
    }
    private Contact getNewTestContact() {
        Contact contact = new Contact();
        contact.setForename("K");
        contact.setSurname("Ak");
        contact.setStreet("289");
        contact.setCity("london");
        contact.setPostalCode(("tw5 0de"));
        contact.setCounty("middlesex");
        contact.setCountry("uk");
        return contact;

    }
    @Test
    void findById() {
        Contact contact = getNewTestContact();
        contactService.save(contact);

        Contact findContact = contactService.findById(contact.getId());

        assertThat(findContact.getId(), is(contact.getId()));

    }
}
