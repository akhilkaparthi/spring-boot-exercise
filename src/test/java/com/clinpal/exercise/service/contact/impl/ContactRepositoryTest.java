package com.clinpal.exercise.service.contact.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItems;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.clinpal.exercise.service.contact.domain.Contact;

/**
 * Tests the ContactRepository
 */
@SpringBootTest
class ContactRepositoryTest {


    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void findContactBySurnameLike() {

        final Contact fred = new Contact();
        fred.setForename("Fred");
        fred.setSurname("Flintstone");
        contactRepository.save(fred);

        final Contact wilma = new Contact();
        wilma.setForename("Wilma");
        wilma.setSurname("Flintstone");
        contactRepository.save(wilma);

        final Contact barney = new Contact();
        barney.setForename("Barney");
        barney.setSurname("Rubble");
        contactRepository.save(barney);

        assertThat(contactRepository.findAll(), hasItems(fred, wilma, barney));

        assertThat(contactRepository.findContactBySurnameLike("Flint%"), hasItems(fred, wilma));
    }

}
