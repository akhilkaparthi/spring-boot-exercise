package com.clinpal.exercise.rest;

import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The REST API for the Contact Service
 */
@RestController
@RequestMapping("/api")
public class ContactResource {

    private final ContactService contactService;

    @Autowired
    public ContactResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    /**
     * REST API to obtain all contacts
     *
     * @return all of the contacts as JSON
     */
    @GetMapping(value = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Contact>> getContacts() {
        return ResponseEntity.ok(contactService.findAll());
    }

    /**
     * REST API to save new contact details
     *
     * @return all of the contacts as JSON
     */
    @PostMapping(value = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Contact> addContact(@RequestBody Contact newContact) {
        return ResponseEntity.ok(contactService.save(newContact));
    }

    /**
     * REST API to update existing contact details
     *
     * @return all of the contacts as JSON
     */
    @PatchMapping(value = "/contacts/{contactId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Contact> updateContact(@PathVariable long contactId, @RequestBody Contact contact) {
        return ResponseEntity.ok(contactService.update(contactId, contact));
    }

    /**
     * REST API to fine existing contact details
     *
     * @return all of the contacts as JSON
     */
    @GetMapping(value = "/contacts/{contactId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Contact> contacts(@PathVariable long contactId) {
        return ResponseEntity.ok(contactService.findById(contactId));
    }

}
