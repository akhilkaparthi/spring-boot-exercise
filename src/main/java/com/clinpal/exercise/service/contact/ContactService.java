package com.clinpal.exercise.service.contact;

import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The contact service
 */
public interface ContactService {


    /**
     * Saves a contact to the database. If the contact does not exist is inserted and it's id is populated.
     *
     * @param contact
     *         the contact to save
     * @return the saved contact
     */
    Contact save(final Contact contact);

    /**
     * Obtains a list of contact with a like match to the supplied
     *
     * @param surname
     *         the wildcard surname of contacts to find
     * @return Obtains a list of matching contacts
     */
    List<Contact> findContactBySurnameLike(final String surname);

    /**
     * Obtains a list of contact all contacts.
     *
     * @return Obtains a list of all contacts
     */
    List<Contact> findAll();

    /**
     * Updates a contact to the database. If the contact does not exist is inserted and it's id is populated.
     *
     * @param contact
     *         the contact to save
     * @return the saved contact
     */
    Contact update(Long id,final Contact contact);

    /**
     * Obtains a list of contact with a  match to the supplied
     *
     * @param id
     *         the wildcard id of contacts to find
     * @return Obtains a list of matching contacts
     */
    Contact findById(Long id);

}
