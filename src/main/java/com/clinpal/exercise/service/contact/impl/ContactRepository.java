package com.clinpal.exercise.service.contact.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/** @author imills */
public interface ContactRepository extends JpaRepository<Contact, Long>, JpaSpecificationExecutor<Contact> {

    /**
     * Obtains a list of contact with a like match to the supplied
     *
     * @param surname
     *         the wildcard surname of contacts to find
     * @return Obtains a list of matching contacts
     */
    List<Contact> findContactBySurnameLike(final String surname);


}
