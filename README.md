# eClinicalHealth Address Book Spring Boot Exercise
## The Exercise
This exercise is a small test to develop a simple address book web application using **REST API** with a **Spring Boot** back-end using **Spring Data JPA Repositories**. A simple development environment has been created for you using the Gradle build system. A skeletal application framework has also been developed that uses Spring Boot, with Spring JPA Repositories against an in memory H2 database. Please extends the example framework and implement the following user stories:

* As a user I want search my address book so that I can find contacts 
* As a user I want to add new contacts to my address book
* As a user I want to update existing contacts in my address book

When developing your solution, bear in mind we expect to see fully documented and tested code.
Upon completion of the exercise, please provide it via GitHub/BitBucket or zip the source and send it to us. Once we have access you your code we will arrange a time for you to present the code to us. We would like you to present your work as if part of an online code review. During the review you should explain your design decisions and talk us through the code you have implemented.

# Setting up the  Development Environment 

## Install Java 8
This test is intended to be developed using the Java 8 JDK. The JDK can be obtained here:
https://www.oracle.com/uk/java/technologies/javase/javase-jdk8-downloads.html 

## Install GIT
GIT is a distributed version control system that is used as the version control system within the project.

1. Obtain and install the GIT version control system from the [Git download page](http://git-scm.com/)
2. Install the application using the downloaded installation program

Verify that GIT is correctly installed by opening a new terminal window and typing:
```
#!bash
git --version
```
A message similar to the following should be displayed:
```
#!bash
git version 2.28.0
```

## Obtaining the Source Code
The framework for the exercise is available from bitbucket https://bitbucket.org/eclinicalhealth/spring-boot-exercise.git. Checkout the project from Bitbucket using the following:
```
#!bash
git clone https://bitbucket.org/eclinicalhealth/spring-boot-exercise.git
```

## Building the software using gradle
To build the software use:

```
#!bash
cd spring-boot-exercise
./gradlew build
```
To perform a clean build:
```
#!bash
./gradlew clean build
```
To Execute All Tests
```
#!bash
./gradlew test
```

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.4/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

